<?php

namespace Xn\JsonEditor;

use Xn\Admin\Admin;
use Xn\Admin\Form;
use Illuminate\Support\ServiceProvider;

class JsonEditorServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(JsonEditorExtension $extension)
    {
        if (! JsonEditorExtension::boot()) {
            return ;
        }

        $this->loadViewsFrom(resource_path('views')."/vendor/xn/json-editor/views", $extension->name);

        $this->registerPublishing($extension);

        Admin::booting(function () {
            Form::extend('json', Editor::class);
        });
    }


    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing(JsonEditorExtension $extension)
    {
        if ($this->app->runningInConsole()) {
            $views = $extension->views();
            $assets = $extension->assets();
            $this->publishes(
                [
                    $views => resource_path('views')."/vendor/xn/json-editor/views",
                    $assets => public_path('vendor/laravel-admin-ext/json-editor')
                ], 
                $extension->name
            );
        }
    }
}
