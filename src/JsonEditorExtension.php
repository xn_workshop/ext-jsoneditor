<?php

namespace Xn\JsonEditor;

use Xn\Admin\Extension;

class JsonEditorExtension extends Extension
{
    public $name = 'laravel-admin-json-editor';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';
}
